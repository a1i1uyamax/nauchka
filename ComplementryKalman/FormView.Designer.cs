﻿namespace ComplementryKalman
{
    partial class FormView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormView));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonX = new System.Windows.Forms.Button();
            this.buttonY = new System.Windows.Forms.Button();
            this.buttonZ = new System.Windows.Forms.Button();
            this.buttonXY = new System.Windows.Forms.Button();
            this.buttonYZ = new System.Windows.Forms.Button();
            this.buttonXZ = new System.Windows.Forms.Button();
            this.buttonXYZ = new System.Windows.Forms.Button();
            this.textBoxK = new System.Windows.Forms.TextBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.labelW = new System.Windows.Forms.Label();
            this.labelQ = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // chart1
            // 
            chartArea1.AxisX.Minimum = 0D;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(12, 12);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "X";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "Y";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "Z";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(474, 155);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // chart2
            // 
            chartArea2.AxisX.Minimum = 0D;
            chartArea2.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart2.Legends.Add(legend2);
            this.chart2.Location = new System.Drawing.Point(12, 173);
            this.chart2.Name = "chart2";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "Legend1";
            series4.Name = "X";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Legend = "Legend1";
            series5.Name = "Y";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Legend = "Legend1";
            series6.Name = "Z";
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Legend = "Legend1";
            series7.Name = "W";
            this.chart2.Series.Add(series4);
            this.chart2.Series.Add(series5);
            this.chart2.Series.Add(series6);
            this.chart2.Series.Add(series7);
            this.chart2.Size = new System.Drawing.Size(477, 155);
            this.chart2.TabIndex = 0;
            this.chart2.Text = "chart1";
            // 
            // buttonX
            // 
            this.buttonX.Location = new System.Drawing.Point(492, 12);
            this.buttonX.Name = "buttonX";
            this.buttonX.Size = new System.Drawing.Size(58, 48);
            this.buttonX.TabIndex = 1;
            this.buttonX.Text = "X";
            this.buttonX.UseVisualStyleBackColor = true;
            // 
            // buttonY
            // 
            this.buttonY.Location = new System.Drawing.Point(556, 12);
            this.buttonY.Name = "buttonY";
            this.buttonY.Size = new System.Drawing.Size(58, 48);
            this.buttonY.TabIndex = 1;
            this.buttonY.Text = "Y";
            this.buttonY.UseVisualStyleBackColor = true;
            // 
            // buttonZ
            // 
            this.buttonZ.Location = new System.Drawing.Point(620, 12);
            this.buttonZ.Name = "buttonZ";
            this.buttonZ.Size = new System.Drawing.Size(58, 48);
            this.buttonZ.TabIndex = 1;
            this.buttonZ.Text = "Z";
            this.buttonZ.UseVisualStyleBackColor = true;
            // 
            // buttonXY
            // 
            this.buttonXY.Location = new System.Drawing.Point(492, 66);
            this.buttonXY.Name = "buttonXY";
            this.buttonXY.Size = new System.Drawing.Size(58, 48);
            this.buttonXY.TabIndex = 1;
            this.buttonXY.Text = "XY";
            this.buttonXY.UseVisualStyleBackColor = true;
            // 
            // buttonYZ
            // 
            this.buttonYZ.Location = new System.Drawing.Point(556, 66);
            this.buttonYZ.Name = "buttonYZ";
            this.buttonYZ.Size = new System.Drawing.Size(58, 48);
            this.buttonYZ.TabIndex = 1;
            this.buttonYZ.Text = "YZ";
            this.buttonYZ.UseVisualStyleBackColor = true;
            // 
            // buttonXZ
            // 
            this.buttonXZ.Location = new System.Drawing.Point(620, 66);
            this.buttonXZ.Name = "buttonXZ";
            this.buttonXZ.Size = new System.Drawing.Size(58, 48);
            this.buttonXZ.TabIndex = 1;
            this.buttonXZ.Text = "XZ";
            this.buttonXZ.UseVisualStyleBackColor = true;
            // 
            // buttonXYZ
            // 
            this.buttonXYZ.Location = new System.Drawing.Point(556, 120);
            this.buttonXYZ.Name = "buttonXYZ";
            this.buttonXYZ.Size = new System.Drawing.Size(58, 48);
            this.buttonXYZ.TabIndex = 1;
            this.buttonXYZ.Text = "XYZ";
            this.buttonXYZ.UseVisualStyleBackColor = true;
            // 
            // textBoxK
            // 
            this.textBoxK.Location = new System.Drawing.Point(535, 174);
            this.textBoxK.Name = "textBoxK";
            this.textBoxK.Size = new System.Drawing.Size(100, 20);
            this.textBoxK.TabIndex = 2;
            this.textBoxK.Text = "0,5";
            this.textBoxK.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(535, 284);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(100, 44);
            this.buttonStart.TabIndex = 3;
            this.buttonStart.Text = "Play/Pause";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // labelW
            // 
            this.labelW.AutoSize = true;
            this.labelW.Location = new System.Drawing.Point(509, 209);
            this.labelW.Name = "labelW";
            this.labelW.Size = new System.Drawing.Size(22, 13);
            this.labelW.TabIndex = 4;
            this.labelW.Text = "Ew";
            // 
            // labelQ
            // 
            this.labelQ.AutoSize = true;
            this.labelQ.Location = new System.Drawing.Point(509, 236);
            this.labelQ.Name = "labelQ";
            this.labelQ.Size = new System.Drawing.Size(20, 13);
            this.labelQ.TabIndex = 4;
            this.labelQ.Text = "Eq";
            // 
            // FormView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 341);
            this.Controls.Add(this.labelQ);
            this.Controls.Add(this.labelW);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.textBoxK);
            this.Controls.Add(this.buttonXZ);
            this.Controls.Add(this.buttonYZ);
            this.Controls.Add(this.buttonXY);
            this.Controls.Add(this.buttonZ);
            this.Controls.Add(this.buttonY);
            this.Controls.Add(this.buttonXYZ);
            this.Controls.Add(this.buttonX);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.chart1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormView";
            this.Text = "Графики погрешностей";
            this.Load += new System.EventHandler(this.FormView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Button buttonX;
        private System.Windows.Forms.Button buttonY;
        private System.Windows.Forms.Button buttonZ;
        private System.Windows.Forms.Button buttonXY;
        private System.Windows.Forms.Button buttonYZ;
        private System.Windows.Forms.Button buttonXZ;
        private System.Windows.Forms.Button buttonXYZ;
        private System.Windows.Forms.TextBox textBoxK;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label labelW;
        private System.Windows.Forms.Label labelQ;
    }
}