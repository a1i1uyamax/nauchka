﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using A1i1uyamax;
using A1i1uyamax.Statistics;


namespace ComplementryKalman
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IFilter filter;
        KAVector W, I;
        KAQuaternion Q;
        KAMatrix R, X0, P0;
        KalmanMode KMode;
        int Zrow;
        double dQ, dWa, dWg, dW, dI, dt, tp;
        double koef;
        ModelFunc f, h;
        bool isCompl;

        public List<double> EqList { set; get; }

        public List<double> EwList { set; get; }

        public List<double> EtList { set; get; }

        public MainWindow()
        {
            InitializeComponent();
            EqList = new List<double>();
            EwList = new List<double>();
            EtList = new List<double>();
        }

        /// <summary>
        /// Всё инициализируем
        /// </summary>
        private void Init()
        {
            dQ = double.Parse(Tb_dQ.Text);
            dWa = double.Parse(Tb_dWa.Text);
            dWg = double.Parse(Tb_dWg.Text);
            //dW = double.Parse(Tb_dW.Text);
            dI = double.Parse(Tb_dI.Text);
            dt = double.Parse(Tb_dt.Text);
            tp = double.Parse(Tb_Tp.Text);

            koef = double.Parse(Tb_K.Text);

            dW = Math.Sqrt(
                (1.0 - koef) * (1.0 - koef) * dWa * dWa
                + koef * koef * dWg * dWg);

            W = new KAVector
            {
                X = double.Parse(Tb_Wx.Text),
                Y = double.Parse(Tb_Wy.Text),
                Z = double.Parse(Tb_Wz.Text)
            };

            Q = new KAQuaternion
            {
                X = double.Parse(Tb_Q1.Text),
                Y = double.Parse(Tb_Q2.Text),
                Z = double.Parse(Tb_Q3.Text),
                W = double.Parse(Tb_Q0.Text)
            };

            I = new KAVector
            {
                X = double.Parse(Tb_Ix.Text),
                Y = double.Parse(Tb_Iy.Text),
                Z = double.Parse(Tb_Iz.Text)
            };

            KMode = GetMode();

            // -----------------------

            // Заполняем нужное для Калмана,
            // исходя из мода
            switch (KMode)
            {
                case KalmanMode.QW_W:

                    isCompl = false;
                    Zrow = 3;
                    X0 = ToMatrix(Q, W);

                    R = R1(dW * dW);
                    f = ModelF1;
                    h = ModelH110;
                    P0 = P1(dQ * dQ, dW * dW);

                    break;

                case KalmanMode.QW_QW:

                    isCompl = false;
                    Zrow = 7;
                    X0 = ToMatrix(Q, W);

                    R = R2(dQ * dQ, dW * dW);
                    f = ModelF1;
                    h = ModelH120;
                    P0 = P1(dQ * dQ, dW * dW);

                    break;

                case KalmanMode.QWI_W:

                    isCompl = false;
                    Zrow = 3;
                    X0 = ToMatrix(Q, W, I);

                    R = R1(dW * dW);
                    f = ModelF2;
                    h = ModelH210;
                    P0 = P2(dQ * dQ, dW * dW, dI * dI);

                    break;

                case KalmanMode.QWI_QW:

                    isCompl = false;
                    Zrow = 7;
                    X0 = ToMatrix(Q, W, I);

                    R = R2(dQ * dQ, dW * dW);
                    f = ModelF2;
                    h = ModelH220;
                    P0 = P2(dQ * dQ, dW * dW, dI * dI);

                    break;

                case KalmanMode.QW_WW:

                    isCompl = true;
                    Zrow = 6;
                    X0 = ToMatrix(Q, W);

                    R = R1ag(dWa * dWa, dWg * dWg);
                    f = ModelF1;
                    h = ModelH11;
                    P0 = P1(dQ * dQ, dW * dW);

                    break;
                case KalmanMode.QW_QWW:

                    isCompl = true;
                    Zrow = 10;
                    X0 = ToMatrix(Q, W);

                    R = R2ag(dQ * dQ, dWa * dWa, dWg * dWg);
                    f = ModelF1;
                    h = ModelH12;
                    P0 = P1(dQ * dQ, dW * dW);

                    break;
                case KalmanMode.QWI_WW:

                    isCompl = true;
                    Zrow = 6;
                    X0 = ToMatrix(Q, W, I);

                    R = R1ag(dWa * dWa, dWg * dWg);
                    f = ModelF2;
                    h = ModelH21;
                    P0 = P2(dQ * dQ, dW * dW, dI * dI);

                    break;
                case KalmanMode.QWI_QWW:

                    isCompl = true;
                    Zrow = 10;
                    X0 = ToMatrix(Q, W, I);

                    R = R2ag(dQ * dQ, dWa * dWa, dWg * dWg);
                    f = ModelF2;
                    h = ModelH22;
                    P0 = P2(dQ * dQ, dW * dW, dI * dI);

                    break;
                default:
                    throw new Exception(
                        "Вы творите какую-то херню, сударь.\n"
                        + "Ф-ция Init().");
            }

            filter =
                new ExtendedKalmanFilter(X0, P0, R, Zrow, dt, f, h);
        }
        
        KalmanMode GetMode()
        {
            int si = Cbox_Mode.SelectedIndex;
            if (si != -1) return (KalmanMode)si;
            return KalmanMode.QW_W;
        }

        private void Bttn_Ok_Click(object sender, RoutedEventArgs e)
        {
            Init();
            EtList.Add(tp);
            Bttn_Check.IsEnabled = true;
            var form = new FormView
            {
                Q = Q,
                W = W,
                I = I,
                X = X0,
                dQ = dQ,
                dW = dW,
                dWa = dWa,
                dWg = dWg,
                dI = dI,
                Tp = tp,
                Koef = koef,
                Kalman = filter,
                KMode = KMode,
                F = f,
                IsCompl = isCompl,

                MW = this
            };
            form.StartInit();

            form.ShowDialog();
        }

        private void Bttn_Check_Click(object sender, RoutedEventArgs e)
        {
            var formcheck = new FormCheck
            {
                EqList = EqList,
                EwList = EwList,
                EtList = EtList,
                dT = filter.dT
            };
            formcheck.ShowDialog();
        }

        #region vsyakaya h**nya

        // функции для R

        static KAMatrix R1ag(double sqr_d_a, double sqr_d_g)
        {
            return KAMatrix.Diag(
                sqr_d_a, sqr_d_a, sqr_d_a, 
                sqr_d_g, sqr_d_g, sqr_d_g);
        }

        static KAMatrix R2ag(double sqr_q, double sqr_d_a, double sqr_d_g)
        {
            return KAMatrix.Diag(
                sqr_q, sqr_q, sqr_q, sqr_q,
                sqr_d_a, sqr_d_a, sqr_d_a,
                sqr_d_g, sqr_d_g, sqr_d_g);
        }

        static KAMatrix R1(double sqr_d)
        {
            return KAMatrix.Diag(sqr_d, sqr_d, sqr_d);
        }

        static KAMatrix R2(double sqr_q, double sqr_d)
        {
            return KAMatrix.Diag(
                sqr_q, sqr_q, sqr_q, sqr_q,
                sqr_d, sqr_d, sqr_d);
        }

        // функции для P0

        static KAMatrix P1(double sqr_q, double sqr_w)
        {
            return KAMatrix.Diag(
                sqr_q, sqr_q, sqr_q, sqr_q,
                sqr_w, sqr_w, sqr_w);
        }
        
        static KAMatrix P2(double sqr_q, double sqr_w, double sqr_i)
        {
            return KAMatrix.Diag(
                sqr_q, sqr_q, sqr_q, sqr_q,
                sqr_w, sqr_w, sqr_w,
                sqr_i, sqr_i, sqr_i);
        }

        // уравнения для F

        public KAMatrix ModelF1(KAMatrix X, double dt, KAVector U)
        {
            return modelFbody(X, dt, U, I);
        }

        public KAMatrix ModelF2(KAMatrix X, double dt, KAVector U)
        {
            return modelFbody(X, dt, U,
                new KAVector(X[7, 0], X[8, 0], X[9, 0]));
        }

        // уравнения для H

        public KAMatrix ModelH110(KAMatrix X, double dt, KAVector U)
        {
            return modelHbody(X, dt, U, I, false, koef, false);
        }

        public KAMatrix ModelH210(KAMatrix X, double dt, KAVector U)
        {
            return modelHbody(X, dt, U,
                new KAVector(X[7, 0], X[8, 0], X[9, 0]), false, koef, false);
        }

        public KAMatrix ModelH120(KAMatrix X, double dt, KAVector U)
        {
            return modelHbody(X, dt, U, I, true, koef, false);
        }

        public KAMatrix ModelH220(KAMatrix X, double dt, KAVector U)
        {
            return modelHbody(X, dt, U,
                new KAVector(X[7, 0], X[8, 0], X[9, 0]), true, koef, false);
        }

        public KAMatrix ModelH11(KAMatrix X, double dt, KAVector U)
        {
            return modelHbody(X, dt, U, I, false, koef);
        }

        public KAMatrix ModelH21(KAMatrix X, double dt, KAVector U)
        {
            return modelHbody(X, dt, U,
                new KAVector(X[7, 0], X[8, 0], X[9, 0]), false, koef);
        }

        public KAMatrix ModelH12(KAMatrix X, double dt, KAVector U)
        {
            return modelHbody(X, dt, U, I, true, koef);
        }

        public KAMatrix ModelH22(KAMatrix X, double dt, KAVector U)
        {
            return modelHbody(X, dt, U,
                new KAVector(X[7, 0], X[8, 0], X[9, 0]), true, koef);
        }

        // body'сы

        private KAMatrix modelHbody(KAMatrix X, double dt, KAVector U,
            KAVector I, bool isQ, double k, bool isCompl = true)
        {
            KAQuaternion q =
                new KAQuaternion(X[1, 0], X[2, 0], X[3, 0], X[0, 0]);
            KAVector w = new KAVector(X[4, 0], X[5, 0], X[6, 0]);

            KARotate.ByAngularVelocities(ref w, ref q, U, I, dt,
                KARotate.NumericalMethods.RK4);

            int Row = isQ ? 7 : 3;
            if (isCompl) Row += 3;

            KAMatrix res = new KAMatrix(Row, 1);
            
            int j = 0;
            if (isQ)
            {
                foreach (double item in q)
                {
                    res[j++, 0] = item;
                }
            }
            if (isCompl)
            {
                foreach (double item in w)
                {
                    res[j++, 0] = (1.0 - k) * item;
                }
                foreach (double item in w)
                {
                    res[j++, 0] = k * item;
                }
            }
            else
            {
                foreach (double item in w)
                {
                    res[j++, 0] = item;
                }
            }
            return res;
        }

        private KAMatrix modelFbody(KAMatrix X, double dt, KAVector U,
            KAVector I)
        {
            KAQuaternion q = 
                new KAQuaternion(X[1, 0], X[2, 0], X[3, 0], X[0, 0]);
            KAVector w = new KAVector(X[4, 0], X[5, 0], X[6, 0]);

            KARotate.ByAngularVelocities(ref w, ref q, U, I, dt,
                KARotate.NumericalMethods.RK4);

            KAMatrix res = new KAMatrix(X.Row, 1);

            int j = 0;
            foreach (double item in q)
            {
                res[j++, 0] = item;
            }
            foreach (double item in w)
            {
                res[j++, 0] = item;
            }
            if (X.Row == 10)
            {
                foreach (double item in I)
                {
                    res[j++, 0] = item;
                }
            }

            return res;
        }

        // трансформим кватры и вектора в матрицу-столбец.
        private static KAMatrix ToMatrix(KAQuaternion q, params KAVector[] v)
        {
            KAMatrix m = new KAMatrix(v.Length * 3 + 4, 1);
            int k = 0;
            foreach (double component in q)
            {
                m[k++, 0] = component;
            }
            for (int i = 0; i < v.Length; i++)
            {
                int j = 0;
                foreach (double component in v[i])
                {
                    m[4 + i * 3 + j++, 0] = component;
                }
            }
            return m;
        }

        #endregion
    }
}
