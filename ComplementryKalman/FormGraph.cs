﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using A1i1uyamax;

namespace ComplementryKalman
{
    public partial class FormGraph : Form
    {
        public FormView FV { set; get; }

        public FormGraph()
        {
            InitializeComponent();
        }

        public void UpdateCharts(KAVector W, KAQuaternion Q, KAMatrix X, double t)
        {
            chart1.Series[0].Points.AddXY(t, W.X);
            chart1.Series[1].Points.AddXY(t, W.Y);
            chart1.Series[2].Points.AddXY(t, W.Z);

            chart2.Series[0].Points.AddXY(t, Q.X);
            chart2.Series[1].Points.AddXY(t, Q.Y);
            chart2.Series[2].Points.AddXY(t, Q.Z);
            chart2.Series[3].Points.AddXY(t, Q.W);

            chart3.Series[0].Points.AddXY(t, X[4, 0]);
            chart3.Series[1].Points.AddXY(t, X[5, 0]);
            chart3.Series[2].Points.AddXY(t, X[6, 0]);

            chart4.Series[0].Points.AddXY(t, X[1, 0]);
            chart4.Series[1].Points.AddXY(t, X[2, 0]);
            chart4.Series[2].Points.AddXY(t, X[3, 0]);
            chart4.Series[3].Points.AddXY(t, X[0, 0]);
        }

        private void FormGraph_Load(object sender, EventArgs e)
        {

        }
    }
}
