﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComplementryKalman
{
    /// <summary>
    /// Определение направления исследования.
    /// </summary>
    public enum KalmanMode : Int32
    {
        /// <summary>
        /// X: Q,W
        /// Z: W
        /// </summary>
        QW_W = 0,

        /// <summary>
        /// X: Q,W
        /// Z: Q,W
        /// </summary>
        QW_QW,

        /// <summary>
        /// X: Q,W,I
        /// Z: W
        /// </summary>
        QWI_W,

        /// <summary>
        /// X: Q,W,I
        /// Z: Q,W
        /// </summary>
        QWI_QW,

        /// <summary>
        /// X: Q,W
        /// Z: Wa, Wg
        /// </summary>
        QW_WW,

        /// <summary>
        /// X: Q,W
        /// Z: Q,Wa, Wg
        /// </summary>
        QW_QWW,

        /// <summary>
        /// X: Q,W,I
        /// Z: Wa, Wg
        /// </summary>
        QWI_WW,

        /// <summary>
        /// X: Q,W,I
        /// Z: Q,Wa, Wg
        /// </summary>
        QWI_QWW
    }
    
}
