﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComplementryKalman
{
    public partial class FormCheck : Form
    {
        public List<double> EqList { set; get; }

        public List<double> EwList { set; get; }

        public List<double> EtList { set; get; }

        public double dT { set; get; }

        public FormCheck()
        {
            InitializeComponent();
        }

        private void FormCheck_Load(object sender, EventArgs e)
        {
            chartEq.ChartAreas[0].AxisX.Minimum = EtList[0];
            chartEw.ChartAreas[0].AxisX.Minimum = EtList[0];
            chartEq.ChartAreas[0].AxisX.Interval = dT;
            chartEw.ChartAreas[0].AxisX.Interval = dT;

            for (int i = 0; i < EtList.Count; i++)
            {
                chartEq.Series[0].Points.AddXY(EtList[i], EqList[i]);
                chartEw.Series[0].Points.AddXY(EtList[i], EwList[i]);
            }
        }
    }
}
