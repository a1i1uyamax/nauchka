﻿namespace ComplementryKalman
{
    partial class FormCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chartEq = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartEw = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chartEq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartEw)).BeginInit();
            this.SuspendLayout();
            // 
            // chartEq
            // 
            chartArea1.Name = "ChartArea1";
            this.chartEq.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartEq.Legends.Add(legend1);
            this.chartEq.Location = new System.Drawing.Point(12, 12);
            this.chartEq.Name = "chartEq";
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Legend = "Legend1";
            series1.Name = "Eq";
            this.chartEq.Series.Add(series1);
            this.chartEq.Size = new System.Drawing.Size(528, 170);
            this.chartEq.TabIndex = 0;
            this.chartEq.Text = "chart1";
            // 
            // chartEw
            // 
            chartArea2.Name = "ChartArea1";
            this.chartEw.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartEw.Legends.Add(legend2);
            this.chartEw.Location = new System.Drawing.Point(12, 188);
            this.chartEw.Name = "chartEw";
            series2.BorderWidth = 2;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Legend = "Legend1";
            series2.Name = "Ew";
            this.chartEw.Series.Add(series2);
            this.chartEw.Size = new System.Drawing.Size(528, 170);
            this.chartEw.TabIndex = 0;
            this.chartEw.Text = "chart1";
            // 
            // FormCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 368);
            this.Controls.Add(this.chartEw);
            this.Controls.Add(this.chartEq);
            this.Name = "FormCheck";
            this.Text = "FormCheck";
            this.Load += new System.EventHandler(this.FormCheck_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartEq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartEw)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartEq;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartEw;
    }
}