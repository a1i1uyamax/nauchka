﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using A1i1uyamax.Statistics;
using A1i1uyamax;
using System.Windows.Forms.DataVisualization.Charting;

namespace ComplementryKalman
{
    public partial class FormView : Form
    {
        /// <summary>
        /// Калман
        /// </summary>
        public IFilter Kalman { set; get; }

        /// <summary>
        /// Кватр
        /// </summary>
        public KAQuaternion Q { set => q = value; get => q; }

        /// <summary>
        /// Велоситисы
        /// </summary>
        public KAVector W { set => w = value; get => w; }

        /// <summary>
        /// Инерция
        /// </summary>
        public KAVector I { set; get; }

        /// <summary>
        /// Исследование, вид
        /// </summary>
        public KalmanMode KMode { set; get; }

        /// <summary>
        /// Система
        /// </summary>
        public KAMatrix X { set; get; }
        
        /// <summary>
        /// Дисперсия кватра
        /// </summary>
        public double dQ { set; get; }

        /// <summary>
        /// Дисперсия велоситисов
        /// </summary>
        public double dW { set; get; }

        /// <summary>
        /// Дисперсия акселей
        /// </summary>
        public double dWa { set; get; }

        /// <summary>
        /// Дисперсия гириков
        /// </summary>
        public double dWg { set; get; }

        /// <summary>
        /// Ошибка инерции
        /// </summary>
        public double dI { set; get; }

        /// <summary>
        /// Коэф комплементарный
        /// </summary>
        public double Koef { set; get; }

        /// <summary>
        /// Период подачи измерений
        /// </summary>
        public double Tp { set; get; }

        /// <summary>
        /// MW
        /// </summary>
        public MainWindow MW { set; get; }

        /// <summary>
        /// Уравнение для X
        /// </summary>
        public ModelFunc F { set; get; }

        /// <summary>
        /// Комплиментарность, наличие в Z
        /// </summary>
        public bool IsCompl { set; get; }

        double t;           // текущее время
        KAMatrix Z;         // измерения
        KAQuaternion q;     // кватр
        KAVector w;         // велоситисы
        bool isQ;           // имеется ли кватр в мещурментах
        double kt;          // счётчик до следующих мищуров
        double _1_Koef;     // 1 - k
        FormGraph form;     // форма с графиками
        double Eq, Ew;

        public FormView()
        {
            InitializeComponent();
            t = 0.0;
            kt = 0.0;
        }

        public void StartInit()
        {
            // делаем вектор Z
            switch (KMode)
            {
                case KalmanMode.QW_W:
                    Z = new KAMatrix(3, 1);
                    isQ = false;
                    break;
                case KalmanMode.QW_QW:
                    Z = new KAMatrix(7, 1);
                    isQ = true;
                    break;
                case KalmanMode.QWI_W:
                    NoisingI(X);
                    goto case KalmanMode.QW_W;
                case KalmanMode.QWI_QW:
                    NoisingI(X);
                    goto case KalmanMode.QW_QW;
                case KalmanMode.QW_WW:
                    Z = new KAMatrix(6, 1);
                    isQ = false;
                    break;
                case KalmanMode.QW_QWW:
                    Z = new KAMatrix(10, 1);
                    isQ = true;
                    break;
                case KalmanMode.QWI_WW:
                    NoisingI(X);
                    goto case KalmanMode.QW_WW;
                case KalmanMode.QWI_QWW:
                    NoisingI(X);
                    goto case KalmanMode.QW_QWW;
                default:
                    Z = null;
                    break;
            }

            // меняем интервалы для наглядности
            if (MW.Chbx_Intervals.IsChecked ?? false && Tp > 0.0)
            {
                chart1.ChartAreas[0].AxisX.Interval = Tp;
                chart2.ChartAreas[0].AxisX.Interval = Tp;
            }

            // ставим в чарты первые точки
            UpdateCharts(W, Q, X, 0.0);

            _1_Koef = 1.0 - Koef;

            Eq = 0.0;
            Ew = 0.0;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // плюсуем дискретное время
            t += Kalman.dT;
            kt += Kalman.dT;

            // узнаём о моментах
            SetK(out KAVector K);

            // крутим теоретическую часть
            KARotate.ByAngularVelocities(ref w, ref q, K, I,
                Kalman.dT, KARotate.NumericalMethods.RK4);

            // крутим систему
            X = F(X, Kalman.dT, K);

            // забиваем измерения значениями
            int a = 0;

            // если не пришли мещурменты, то Z = Х
            if (kt < Tp)
            {
                if (isQ)
                {
                    a = 4;
                    for (int i = 0; i < 4; i++)
                    {
                        Z[i, 0] = X[i, 0];
                    }
                }
                if (IsCompl)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        Z[i + a, 0] = _1_Koef * X[i + 4, 0];
                        Z[i + 3 + a, 0] = Koef * X[i + 4, 0];
                    }
                }
                else
                {
                    for (int i = 0; i < 3; i++)
                    {
                        Z[i + a, 0] = X[i + 4, 0];
                    }
                }
            }
            // если пришли, то шумим теорию и пихаем в Z
            else
            {
                if (isQ)
                {
                    foreach (double item in Q)
                    {
                        Z[a++, 0] = item;
                    }
                }
                if (IsCompl)
                {
                    foreach (double item in W)
                    {
                        Z[a, 0] = _1_Koef * item;
                        Z[a++ + 3, 0] = Koef * item;
                    }
                }
                else
                {
                    foreach (double item in W)
                    {
                        Z[a++, 0] = item;
                    }
                }
                kt = 0.0;
                NoizingZ(ref Z, isQ);
            }

            if (isQ) Z = QToNormal(Z);

            // следующий шаг Калмана и нормализуем кватр
            X = Kalman.Step(Z, K, X);
            X = QToNormal(X);

            // обновляем форму графиков, если есть
            if (form != null) form.UpdateCharts(W, Q, X, t);

            // обновляем чарты и форму графиков, если она есть
            if (kt == 0.0) UpdateCharts(W, Q, X, t);

            Ew += (Math.Abs(X[4, 0] - W.X) + Math.Abs(X[5, 0] - W.Y)
                + Math.Abs(X[6, 0] - W.Z)) / 3.0;
            Eq += (Math.Abs(X[1, 0] - Q.X) + Math.Abs(X[2, 0] - Q.Y)
                + Math.Abs(X[3, 0] - Q.Z) + Math.Abs(X[0, 0] - Q.W)) / 4.0;
        }

        /// <summary>
        /// Нормализация кватра в векторе динамики системы
        /// </summary>
        /// <param name="X"></param>
        /// <returns></returns>
        public KAMatrix QToNormal(KAMatrix X)
        {
            var q = new KAQuaternion(
                X[1, 0], X[2, 0], X[3, 0], X[0, 0]
                ).ToNormal();
            var m = new KAMatrix(X);
            int i = 0;
            foreach (double item in q)
            {
                m[i++, 0] = item;
            }
            return m;
        }

        /// <summary>
        /// Зашумить Z
        /// </summary>
        /// <param name="Z"></param>
        /// <param name="isQ"></param>
        public void NoizingZ(ref KAMatrix Z, bool isQ)
        {
            int it = 0;
            // если есть Q, то шумим его.
            if (isQ)
            {
                it = 4;
                noizingBody(0, 4, ref Z, dQ);
            }
            if (IsCompl)
            {
                noizingBody(it, it + 3, ref Z, dWa);
                noizingBody(it + 3, it + 6, ref Z, dWg);
            }
            else
            {
                var m = new KAMatrix(6, 1);
                for (int i = 0; i < 3; i++)
                {
                    m[i, 0] = Z[it + i, 0];
                    m[i + 3, 0] = Z[it + i, 0];
                }
                noizingBody(0, 3, ref m, dWa);
                noizingBody(3, 6, ref m, dWg);
                for (int i = 0; i < 3; i++)
                {
                    Z[i + it, 0] =
                        _1_Koef * m[i, 0] + Koef * m[i + 3, 0];
                }
            }
        }

        private void SetK(out KAVector K)
        {
            double b = Convert.ToDouble(textBoxK.Text);

            if (buttonX.Capture) K = new KAVector(b, 0.0, 0.0);
            else if (buttonXY.Capture) K = new KAVector(b, b, 0.0);
            else if (buttonXYZ.Capture) K = new KAVector(b, b, b);
            else if (buttonXZ.Capture) K = new KAVector(b, 0.0, b);
            else if (buttonY.Capture) K = new KAVector(0.0, b, 0.0);
            else if (buttonYZ.Capture) K = new KAVector(0.0, b, b);
            else if (buttonZ.Capture) K = new KAVector(0.0, 0.0, b);
            else K = KAVector.NULL;
        }

        public void UpdateCharts(KAVector W, KAQuaternion Q, KAMatrix X, double t)
        {
            chart1.Series[0].Points.AddXY(t, Math.Abs(X[4, 0] - W.X));
            chart1.Series[1].Points.AddXY(t, Math.Abs(X[5, 0] - W.Y));
            chart1.Series[2].Points.AddXY(t, Math.Abs(X[6, 0] - W.Z));

            chart2.Series[0].Points.AddXY(t, Math.Abs(X[1, 0] - Q.X));
            chart2.Series[1].Points.AddXY(t, Math.Abs(X[2, 0] - Q.Y));
            chart2.Series[2].Points.AddXY(t, Math.Abs(X[3, 0] - Q.Z));
            chart2.Series[3].Points.AddXY(t, Math.Abs(X[0, 0] - Q.W));
        }

        //private void AntiLag(Chart chart, bool E = false)
        //{

        //    for (int i = 0; i < chart.Series.Count; i++)
        //    {
        //        chart.Series[i].Points.RemoveAt(0);
        //    }
        //    chart.ChartAreas[0].AxisX.Minimum += Kalman.dT;
        //    chart.ChartAreas[0].AxisX.Maximum += Kalman.dT;

        //    chart.ChartAreas[0].AxisX.LabelStyle.Format = "{0:0.0}";

        //    chart.ChartAreas[0].AxisY.LabelStyle.Format =
        //        E ? "{0:0.000E00}" : "{0:0.0000}";


        //    var max = new double[chart.Series.Count];
        //    var min = new double[chart.Series.Count];

        //    for (int i = 0; i < chart.Series.Count; i++)
        //    {
        //        max[i] = chart.Series[i].Points.FindMaxByValue().YValues[0];
        //        min[i] = chart.Series[i].Points.FindMinByValue().YValues[0];
        //    }

        //    for (int i = 1; i < chart.Series.Count; i++)
        //    {
        //        if (max[i] > max[i - 1]) max[0] = max[i];
        //        if (min[i] < min[i - 1]) min[0] = min[i];
        //    }

        //    chart.ChartAreas[0].AxisY.Maximum = max[0];
        //    chart.ChartAreas[0].AxisY.Minimum = min[0];

        //}

        private void Noising(ref KAMatrix res, double disQ, double disW)
        {
            int j = 0;
            if (disQ > 0.0)
            {
                noizingBody(0, 4, ref res, disQ);
                var q = new KAQuaternion(
                    res[1, 0], res[2, 0], res[3, 0], res[0, 0]
                    ).ToNormal();
                int i = 0;
                foreach (double item in q)
                {
                    res[i++, 0] = item;
                }
                j = 4;
            }
            noizingBody(j, 3 + j, ref res, disW);
        }

        public void NoisingI(KAMatrix X)
        {
            var xI = new KAMatrix(3, 1);
            noizingBody(0, 3, ref xI, dI);
            X[7, 0] += xI[0, 0];
            X[8, 0] += xI[1, 0];
            X[9, 0] += xI[2, 0];
        }

        private void noizingBody(int i0, int iMax, ref KAMatrix res,
            double dis)
        {
            int length = 20;
            for (int i = i0; i < iMax; i++)
            {
                double m = 0.0;
                for (int j = 0; j < length; j++)
                {
                    m += 2.0 * new Random(
                        DateTime.Now.Millisecond + j * 32 + i
                        ).NextDouble()
                        * dis - dis;
                }
                res[i, 0] += m / length;
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            timer1.Enabled ^= true;
            if (!timer1.Enabled)
            {
                double eq = Eq / (t / Kalman.dT);
                double ew = Ew / (t / Kalman.dT);
                //if (labelQ.Text != "Eq" && labelW.Text != "Ew")
                {
                    MW.EqList.Add(eq);
                    MW.EwList.Add(ew);
                }
                labelQ.Text = "Eq = " + eq.ToString();
                labelW.Text = "Ew = " + ew.ToString();
            }
        }

        private void FormView_Load(object sender, EventArgs e)
        {
            if (MW.Chbx_SystemGraph.IsChecked ?? false)
            {
                form = new FormGraph
                {
                    FV = this
                };
                form.Show();
                form.FormClosed += (s, arg) => Close();
                form.UpdateCharts(W, Q, X, 0.0);
                this.FormClosed += (s, arg) => form.Close();
            }
        }
    }
}
